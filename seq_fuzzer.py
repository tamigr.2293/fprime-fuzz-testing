from fuzzingbook.Fuzzer import RandomFuzzer
from fprimegdsrunner import FprimeGDSRunner
import random
import requests
import json


class RandomSequenceFuzzer(RandomFuzzer):
    def __init__(self, min_length=10, max_length=100,
                 char_start=32, char_range=32, seq_size=1):
        RandomFuzzer.__init__(self, min_length, max_length, char_start, char_range)
        self.n_cmds = seq_size

        # Get list of available commands
        cmds_json_url = "http://127.0.0.1:5000/dictionary/commands"
        headers = {
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:89.0) Gecko/20100101 Firefox/89.0'
        }
        html_response = requests.get(cmds_json_url, headers)
        self.cmds_json = json.loads(html_response.text)
        self.avail_cmds = list(self.cmds_json.keys())
        self.avail_cmds.remove('pingRcvr.PR_StopPings')

    def generate_seq(self):
        """
        Generates a random commands' sequence. It requires interaction with the web browser UI elements.
        :return: seq: List. Contains the random commands and parameters generated.
        """
        random.seed()
        seq = []

        # Iterate through elements in a sequence. An element is a particular command and its parameters.
        for i in range(self.n_cmds):
            cmd_params = []
            # Get number of commands available
            n_cmds = len(self.avail_cmds)

            # Choose command index
            random_ind = random.randint(0, n_cmds-1)

            # Get command name by index
            cmd_name = self.avail_cmds[random_ind]

            # Add to list
            cmd_params.append(cmd_name)

            # Get number of params
            # NOTE: ALL PARAMETERS WITH TYPE != ENUM MUST BE ADDED AS A STRING
            for arg in self.cmds_json[cmd_name]["args"]:
                # If param is Enum type, choose random index
                if arg["type"] == 'Enum':
                    # Choose random index
                    possible_options = len(arg["possible"])
                    random_opt = random.randint(0, possible_options-1)
                    # Create tuple (<parameter name>, <random index value>)
                    param_opt = (arg["name"], random_opt)
                # Else, create tuple (<parameter name>, <random string value>)
                else:
                    param_opt = (arg["name"], self.fuzz())
                cmd_params.append(param_opt)
            seq.append(cmd_params)
        return seq

    def run(self, runner=FprimeGDSRunner):
        """
        Run 'runner' with fuzzed sequence of commands and parameters.
        :param runner: Class.
        :return: seq_info: Tuple. Contains information of each sequence and the entire process execution.
        """
        seq = self.generate_seq()
        total_time, relevant_acts = runner.run_process(seq)
        seq_info = (seq, total_time, relevant_acts)
        print("seq_info: ", seq_info)
        return seq_info
