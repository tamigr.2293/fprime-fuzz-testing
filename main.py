from seq_fuzzer import RandomSequenceFuzzer
from fprimegdsrunner import FprimeGDSRunner
import time
from selenium.webdriver import Firefox
import matplotlib.pyplot as plt
import argparse
import json
import os


def to_json(outcomes, total_time, path):
    """
    Write execution information to JSON file.
    :param outcomes: List. Contains (seq, total_time, relevant_acts) tuple. This is the execution info.
    :param total_time: Int. Total execution time of the entire process.
    :param path: Str. Main directory that contains JSON files.
    :return:
    """
    json_dict = {}

    # Sequence information
    json_list = []
    number_of_seqs = len(outcomes)
    for sequence in outcomes:
        sequence_dict = dict()

        # Add information of each command sent
        commands_list = []
        commands_info = sequence[0]
        number_of_cmds = len(commands_info)
        user_time = sequence[1]
        relevant_events = sequence[2]
        commands_names = [command_params[0] for command_params in commands_info]
        parameters = [command_params[1:] for command_params in commands_info]

        for i in range(len(commands_names)):
            cmd_dict = dict()
            params_dict = {}
            cmd_dict["cmd_name"] = commands_names[i]
            for param in parameters[i]:
                param_name = param[0]
                param_val = param[1]
                params_dict[param_name] = param_val
            cmd_dict["params"] = params_dict
            commands_list.append(cmd_dict)
        sequence_dict["cmds"] = commands_list
        sequence_dict["user time"] = user_time
        sequence_dict["relevant events"] = relevant_events
        json_list.append(sequence_dict)
    print(json_list)
    json_dict["sequence information"] = json_list

    # Add information of the entire process
    exec_dict = {}
    exec_dict["total time"] = total_time
    json_dict["total execution information"] = exec_dict

    # Write to json
    number_of_seqs_dir = path + str(number_of_seqs)
    number_of_commands_str = str(number_of_cmds) + "_cmds"
    filename = "/" + number_of_commands_str + "-" + time.strftime("%Y%m%d_%H%M%S") + '.txt'
    json_path = number_of_seqs_dir

    if not os.path.exists(json_path):
        os.makedirs(json_path)
    with open(json_path + filename, 'w') as outfile:
        json.dump(json_dict, outfile, indent=2, separators=(',', ': '))


def plot_seq_user_time(json_path):
    with open(json_path) as results_file:
        results = json.load(results_file)
        print("RESULTS")
        print(results)
        user_times = [sequence["user time"] for sequence in results]
        number_of_seqs = len(results)
        seq_n = list(range(number_of_seqs))
        plt.plot(seq_n, user_times)
        plt.xlabel('Sequence number (index)')
        plt.ylabel('Time')
        plt.title('Time of each sequence execution')

        figure_path = "plots/user_time/" + str(number_of_seqs) + "/"
        if not os.path.exists(figure_path):
            os.makedirs(figure_path)

        filename = json_path.split("/")[-1]
        filename = filename.split(".")[0] + ".png"

        plt.savefig(figure_path + filename)


def run_experiment(random_fuzzer, n_seqs):
    """
    Create a random fuzzer instance to execute flight software with random input.
    :param random_fuzzer: Class. Fuzzer.
    :param n_seqs: Int. Number of sequences.
    :return: process_time_list: List. Contains (seq, total_time, relevant_acts) tuple. This is the execution info.
    """
    driver = Firefox()
    driver.get("http://127.0.0.1:5000/")
    process_time_list = random_fuzzer.runs(FprimeGDSRunner(driver), n_seqs)
    driver.quit()
    return process_time_list


def get_parameters():
    """
    Parse script arguments.
    Every path argument must end with a "/".
    """
    parser = argparse.ArgumentParser(prog='run_experiment.py')

    parser.add_argument('--json_path', type=str, default='results/', help="Save JSON reports in this directory")
    parser.add_argument('--n_seqs', type=int, default=250, help="Number of sequences")
    parser.add_argument('--n_cmds', type=int, default=5, help="Number of commands in a sequence")
    parser.add_argument('--min_length', type=int, default=1, help="Minimum length of the random command names.")
    parser.add_argument('--max_length', type=int, default=21, help="Maximum length of the random command names.")
    parser.add_argument('--char_start', type=int, default=33, help="Index of the range that indicates where to start "
                                                                   "producing random command names in ASCII code.")
    parser.add_argument('--char_range', type=int, default=93, help="Length of the characters range in ASCII code.")

    return parser.parse_args()


def main(json_path, n_seqs, n_cmds, min_length, max_length, char_start, char_range):
    """
    :param json_path: Str. Directory for JSON reports. The directory must exist. Must end with a "/" character.
    :param n_seqs: Int. Number of sequences.
    :param n_cmds: Int. Number of commands per sequence.
    :param min_length: Int. Minimum length of the random command names.
    :param max_length: Int. Maximum length of the random command names.
    :param char_start: Int. Index of the range that indicates where to start producing random command names in ASCII code.
    :param char_range: Int. Length of the characters range in ASCII code.
    :return:
    """
    random_fuzzer = RandomSequenceFuzzer(min_length=min_length, max_length=max_length, char_start=char_start,
                                         char_range=char_range, seq_size=n_cmds)

    time_exec_init = time.time()
    outcome_list = run_experiment(random_fuzzer, n_seqs)
    total_exec_time = time.time() - time_exec_init

    # Write outcome information report
    to_json(outcome_list, total_exec_time, json_path)


if __name__ == "__main__":
    if __name__ == "__main__":
        args = get_parameters()
        main(args.json_path, args.n_seqs, args.n_cmds, args.min_length, args.max_length, args.char_start, args.char_range)