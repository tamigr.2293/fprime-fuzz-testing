from fuzzingbook.Fuzzer import Runner
import time


class FprimeGDSRunner(Runner):
    def __init__(self, driver):
        """Initialize"""
        Runner.__init__(self)
        self.driver = driver

    def run_process(self, seq):
        """
        Sends commands from F Prime GDS UI to the Ref application of FPrime software. It also requires interaction with
        the web browser UI elements.
        :param seq: List. Contains the random commands and parameters generated.
        :return: (total_time, relevant_acts): Tuple. Information of the sequence execution: time and found high severity
        events with their corresponding description.
        """
        init_time = time.time()
        print("Sending sequence: ")
        print(seq)
        for elem in seq:
            time.sleep(1)
            # Click dropdown list
            dropdown = self.driver.find_elements_by_id("mnemonic")[0]
            dropdown.click()
            # Get elements list
            time.sleep(1)
            li_elems = self.driver.find_elements_by_tag_name("li")
            li_names = [element.text for element in li_elems]

            #Debug ValueError: name is not in list
            print("li_elems: ")
            print(li_elems)
            print("li_names: ")
            print(li_names)
            # Select command
            cmd_index = li_names.index(elem[0])
            cmd = li_elems[cmd_index]
            cmd.click()

            # Iterate through parameters. NOTE: the first element is the command
            if len(elem) > 1:
                for i in range(1, len(elem)):
                    arg_name = elem[i][0]
                    arg_val = elem[i][1]
                    selection_element = self.driver.find_element_by_id(arg_name)
                    # The argument value must be chosen through a dropdown list
                    if type(arg_val) == int:
                        # Display argument dropdown list
                        selection_element.click()
                        # Click chosen option
                        all_options = self.driver.find_elements_by_class_name("vs__dropdown-option")
                        option = all_options[arg_val]
                        option.click()
                    # The argument value must be fed as input
                    else:
                        # Write input in the parameter field
                        selection_element.send_keys(arg_val)
            # Send command and parameters
            send_button = self.driver.find_elements_by_class_name("col-2.btn.btn-primary")[0]
            send_button.click()
        time.sleep(2)
        # Click events tab to get status
        events_tab = li_elems[li_names.index("Events")]
        events_tab.click()

        # Check if there is any fatal status
        table = self.driver.find_elements_by_class_name("sortable.table.table-bordered.table-hover")
        table_body = table[2].find_element_by_xpath(".//tbody")

        relevant_acts = []
        for row in table_body.find_elements_by_xpath(".//tr"):
            severity = row.find_elements_by_xpath(".//td")[3].text
            if "FATAL" in severity or "WARNING_HI" in severity:
                relevant_acts.append((severity, row.find_elements_by_xpath(".//td")[4].text))

        # Clear table
        clear_button = self.driver.find_elements_by_class_name("col-3.btn.btn-secondary")[0]
        clear_button.click()

        # Return to commanding tab
        events_tab = li_elems[li_names.index("Commanding")]
        events_tab.click()

        total_time = time.time() - init_time

        return total_time, relevant_acts

